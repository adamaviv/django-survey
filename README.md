# Django Installation

This is a python3 project. You will need to install the django pacakge for python 3.4+.

The tutorial on Django is pretty awesome and a great resource

* https://docs.djangoproject.com/en/2.0/intro/tutorial01/

If this is your first time running django locally, you need to makemigrations
and migrate.

For more information about the `survey` application, see `mysite/survey/README.md`

## Take the sample survey locally

First go to: (the query string will be different for mturk

http://127.0.0.1:8000/mturk?assignmentId=aaa&hitId=aaa&turkSubmitTo=http%3A%2F%2F127.0.0.1%3A8000&workerId=aaaa

Follow the link to take the survey. Come back and enter your code when done. 

## File Break Downs

Here's some of the important files and directories.

```
mysite/ : Project Folder
 |
 |- README.md      : README documentation (this file)
 |- manage.py      : django script for managing the project
 |- static/        : static directory
 |- mysite/        : project settings folder
 |   |
 |   |-urls.py     : dictates which app urls are served
 |   |-settings.py : settings file for the project
 |   .
 |   :
 |
 '-survey/ : django applications
     |
     |- README.md   : Documentation for the survey application
     |- views.py    : function hooks to server different pages of the web app for GET/POST requests
     |- urls.py     : mapping of urls to views function hooks
     |- models.py   : database/storage model setup
     |- forms.py    : where entry forms are defined
     |- templates/  : folder of html templates that can be served (e.g., dynamic content served content)
     |  |
     |  '-README.md : Documenation for the templates
     |- static/     : static content that does not change (e.g., css, js)
     |  |
     |  '-survey/   : general statics folder
     |    |
     |    |-images/ : Folder for image static files
     '- migrations/ : folder containing database migrations when the model file cahnges
```

## Local Development

### Database

The local development uses a sqlite3 instance. This is part of the `settings.py`
file. When you make changes to `models.py`, this requires updates to the backend
database. You must first reate the migrations

```
./manage.py makemigrations
```

This will create a set of `.py` files in `survey/migrations` which will update
the database. The actually updates occur via

```
./manage.py migrate
```

You _should not_ add the migrations in the reposiotiry, nor the sqlite
database. Each local instance can migrate and build its own. Further, on the
deployed instance, we will host via `mysql`. 


### Collect Static

If you create new static files, you need to collect them in the main static location

```
./manage.py collectstatic
```

This may overwrite some things, but that's ok.

### Running the server instance

To run the server use the following command (I've set the environment to python3)

```
./manage.py runserver
```

This will run a server instance and provide a local URL `http://127.0.0.1:8000/`
that you can access and try out the system.

After a pull, you should migrate before running the server.

