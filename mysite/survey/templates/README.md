# Templates and Bootstrap

Templates are used to render the HTML of a page. Ther eis a built in Django
language for templates that you can review here:
https://docs.djangoproject.com/en/2.0/ref/templates/builtins/

Additionally, to make CSS styling simpler, I've used bootstrap to do the
styling. You can see the bootstrap documenation here:
https://getbootstrap.com/docs/4.1/getting-started/introduction/


## Typical Page Template

A typical page loads a header template and the main page, as well as the
bootstrap CSS. For example

```
{% extends "base.html" %}


{% block content %}
<!-- place all content here -->
{% endcontent %}
```

The `base.html` file contains the primary structure of the document, and all
other pages inherit from it. Make edits there if needed be. It automatically
load `jquery`, `bootsrap` and `popper`.

For pages with a output and progress, use the base `base_optout.html`

## Form Templates

Forms are more complicated as there are built in Django tmeplates for rendering
form elements, such as radio buttons. I do not like how Django does this, so I
came up with a simple way to render the buttons nicer, and with boostrap
classes.

Take, for example, the relevant part of the template for the demographic questoins:

```
            <form action="/" method="post">{% csrf_token %}
                <ol>
                    <li> {% include "radio.html" with field=form.age %} </li>
                    <li> {% include "radio.html" with field=form.gender %} </li>
                    <li> {% include "radio.html" with field=form.handed %} </li>
                    <li> {% include "radio.html" with field=form.locale %} </li>
                </ol>
                <input class="btn btn-outline-primary" type="submit" value="Submit" >
            </form>
```

Each of these includes, takes a field of the form, eg., a question about age,
and sets that in a sub template. The `radio.html` template can then render that
question/field nicely with boostrap, with the label and each of the options
properly wrapped in the correct div class.

```
<strong> {{field.label}} </strong>
{% for opt in field %}
<div class="form-check">
    {{opt.tag}}
    <label class="form-check-label" for="{{opt.id_for_label}}">
        {{ opt.choice_label}}
    </label>
</div>
{% endfor %}
<br>
```

The form object are defined in `forms.py`

## Dependent Questions

If you want to add a question that depends on the answer of another, use the
`radio_depend.html` template. You provide it the field name and choice it
depends on. 
```
                    <ol type="a">
                      {% include "radio_depend.html" with field=dateForm.datetype depend="datepatternother" choice="0" dependents="whichyear whichday" %}
                      <ol type="i">
                        {% include "radio_depend.html" with field=dateForm.whichyear depend="datetype" choice="0"%}
                        {% include "radio_depend.html" with field=dateForm.whichday depend="datetype" choice="1"%}
                      </ol>
                    </ol>

```

Here `dateForm.dateType` will appear when `datepatternother` choice 0 is
selected, and will disappear when it is deselected. Similarly the
`dateform.whichyear` and `dateFrom.whichday` depend on choice 0 and choice 1,
respectively, of the `dateform.datetype` selection.

Since the two sub question, both `dateForm.whichyear` and `dateForm.whichday`
are depends of `dateform.datetype`, that is, when `dateForm.datetype` is not
display neither should `dateForm.whichyear` and `dateForm.whichday`, then we can
list these ad `dependents` in the template. That will undisplay them if the
parent is undisplayed.


