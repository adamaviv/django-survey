# Survey Application

The survey django application is setup to be used with Mturk as an external
question survey. It can be easily customized with new questions and pages. 

## `urls.py`

There are two main entry points to the survey:

 * `/mturk` : used on mturk and displayed in the frame on mturk
 * `/` : points to the index() view, which uses the progress counter to go to navigate to the right page

## The Model

There are two models, located in `models.py` -- see there for more information
about the specific fields.

 * `Participant` : This stores information about the participant and is designed
   to be removed latter to leave a de-identified dataset --- thus no use of foreign keys
    
 * `Results` : Stores relevant result information for the survey. Add to this if
   you want to expand the survey
   
## Sessions

The application uses Django's session middleware to track participants in the
survey. Stored in the session is the `startcode`, which is used to look up the
`Participant` and the `Results` objects for that given user. 

As this occurs often, looking up the session info and the associated objects,
there is a function decorator `add_session_info` to handle this and set those
values for you, or make appropriate error returns when not available. 

There shouldn't need to be additional information stored in the session other
than the `startcode`. Tracking information for survey results should be added to
the `Results` model.

When a participant opts-out, the session is cleared. 

The session length is set to 1 hour from the last login.

## View Ordering

A variable `view_order` describes the order of the views to be displayed in the
survey. These are indexed based on `Participant.progress`. By default, progress
0 is always `login`, so this is labeled `None`. The last view in the
`view_order` should be `finish`, which will display and/or generate the
`fincode` if needed.

In `index` view, the process of the participant is queried, and the appropriate
view is called to return render results. 

If the progress is beyond the view order, the session is cleared. This is an
error condition and should not happen.


## Posting and Getting on a view

In general, each view in the survey has a `POST` and `GET` response. 

A `GET` response is used to render the page's template with the standard
information. This would be displaying a form of questions. When the user whishes
to proceed, such as by submitting information, the page will submit with method
`POST` and `action` as `\` --- which directs back to `index()` and will post
back to the given view.

When the method is `POST`, the view will look for relevant posted info, say in a
form, and update the results. If it's all good, the `Participant.progess` is
updated, and the user is redirected back to `index()` to get the next page in the survey. 

## Forms

The forms of the survey are defined in `forms.py`. Each view that asks questions
has an associated form.  Forms are rendered using Django's renderer and
templates. See `templates/README.md` for more information about how to do that.

## Templates

Templates are html or other pages that should be rendered with some
variables. Django provides a template language for this. Template are found in
`templates/`. A longer description of the templates of this application is
found in `survey/templates/README.md`.


## Statics

Statics are for pages that do not need to be rendered, e.g., raw html and or
javascript. Place all relevant statics in `static/survey/`. Image statics go in `static/survey/image`. 

To load a static in a templates.

## Expanding the survey

To expand the survey, you will need to complete the following steps:

  1. Create a new view function that takes the following argument's
     ```python
         def view(request,startcode,participant,result)
     ```
     
  2. Add the view to the `view_order` in the correct place
  
  3. The view should have action for `POST` and `GET`
  
  4. Add a new form to `forms.py`
  
  5. Create a template for that view (See `templates/README.md` for more info)
  
  6. Expand the `Results` in models.py to store additional results elements.
  

## Customizing the Jumbo Header

Edit the following template files to customize the jumbo header to match the study title

 * `icon.html` : src-img to the left of header
 * `department.html` : main header 
 * `title.html` : the study title
 * `pi.html` : the principal investigator
 * `institution.html` : the institution
 * `oversight.html` : the oversight link
    
Comment out any header information you don't want in the following templates

 * `jumbo_header_no_opt_out.html` : header w/o opt out option and progress bar
 * `jumbo_header_w_opt_out.html` : header w/ opt out option and progress bar
